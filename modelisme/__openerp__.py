# -*- coding: utf-8 -*-
{
    'name': "modelisme",

    'summary': """
        Manage composition of build products""",

    'description': """
        Manage composition of self made products
        to handle costs, stock, ...
    """,

    'author': "NSe",
    'website': "http://www.seinlet.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}
